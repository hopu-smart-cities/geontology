certifi==2021.10.8
charset-normalizer==2.0.10
click==8.0.3
loguru==0.5.3
ping3==3.0.2
urllib3==1.26.8
