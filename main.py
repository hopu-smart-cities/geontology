# This is a sample Python script.
import json
import os
import subprocess

from ping3 import ping
import urllib.request


def _get_ip_by_host_dns():
    t = subprocess.Popen(['host', 'myip.opendns.com', 'resolver1.opendns.com'], stdout=subprocess.PIPE)
    print(f"{t.communicate()}")


def _get_ping(times=4):
    _ping = 0
    for i in range(0, times):
        _ping += ping('2.139.202.9', unit='ms')

    return _ping / times


def _get_public_ip():
    return urllib.request.urlopen('https://ident.me').read().decode('utf8')


if __name__ == '__main__':
    iexec_out = os.environ['IEXEC_OUT']
    response = {'ping_time': _get_ping(), 'public_ip': _get_public_ip()}

    print(f'{response}')

    # Append some results in /iexec_out/
    with open(iexec_out + '/result.txt', 'w+') as fout:
        fout.write(json.dumps(response))

    # Declare everything is computed
    with open(iexec_out + '/computed.json', 'w+') as f:
        json.dump({"deterministic-output-path": iexec_out + '/result.txt'}, f)
