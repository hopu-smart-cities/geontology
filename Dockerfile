FROM python:3.7.3-alpine3.10
COPY . ./app

RUN apk add bind-tools
RUN pip install -r ./app/requirements.txt

ENTRYPOINT ["python3", "./app/main.py"]
